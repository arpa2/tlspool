#! /bin/sh
#
# Runs the test EXE ($1, which should be the path to the "onlinecheck"
# test-executable), and can use SRCDIR ($2, the start of the source tree)
# for information and RUNDIR ($3, somewhere in the build path) for
# temporary artifacts.
#
# Copy the root.key so it does not get damaged by the test, and adjust
# the configuration file to point to the copy.

EXE="$1"
SRCDIR="$2"
RUNDIR="$3"

test -d "$RUNDIR" || exit 1

#NOW_CONFIG# ROOTKEY="../etc/root.key"
#NOW_CONFIG# CONF="../etc/tlspool.conf"
#NOW_CONFIG# test -f "$ROOTKEY" || exit 1
#NOW_CONFIG# test -f "$CONF" || exit 1

#NOW_CONFIG# cp "$ROOTKEY" "$RUNDIR/root.key" || exit 1
#NOW_CONFIG# sed "s+^dnssec_rootkey.*+dnssec_rootkey $RUNDIR/sroot.key+" < "$CONF" > "$RUNDIR/tlspool.conf" || exit 1
#NOW_CONFIG# test -s "$RUNDIR/tlspool.conf" || exit 1

"$EXE" "$RUNDIR/tlspool.conf" "$SRCDIR/test/tlspool-test-client-cert.der" "$SRCDIR/test/tlspool-test-client-pubkey.pgp" | tee | grep -v 'UNEXPECTED OUTPUT FAILURE'
